

class InvalidRequestError(Exception):
    """Raised on invalid request received from HTTP clients"""
    pass
