# -*- coding: utf-8 -*-

from django.utils import unittest as unittest
from django.test.client import Client


class StatusPageTest(unittest.TestCase):
    def test_status_page(self):
        client = Client()
        response = client.get('/status_page')
        self.assertEqual(response.status_code, 200)
        self.assertTrue("Status OK" in response.content)
