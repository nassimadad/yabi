.. _installation:

Installation
============


Dependencies
------------

- Python
- python include headers
- Postgresql is preferred, but other database supported by Django should also work


Source code
-----------

Yabi source code is available on Bitbucket:

   ``$ hg clone https://bitbucket.org/ccgmurdoch/yabi``


How to install
--------------

.. toctree::
    :maxdepth: 2
   
    apache
    settings
    database
    caching
    yabish




